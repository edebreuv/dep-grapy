# Copyright CNRS/Inria/UCA
# Contributor(s): Eric Debreuve (since 2019)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from brick.figure import NETWORKX_LAYOUT_SUFFIX
from brick.app.defaults import DEFAULT_SAVING_FORMAT, MAX_N_GRAPHS, MAX_N_NODES, MIN_N_NODES
from brick.graph.output import GRAPH_FORMATS


_graph_formats = ", ".join(GRAPH_FORMATS.keys())

USAGE = f"""PURPOSE:
Print, show, and/or save dependency graphs (module, class, function/method) of Python projects. These graphs might be composed of several connected components (CCs) [1] which are then shown/saved separately.

USAGE:
python3 dep-grapy.py project_folder [options...]
where each option can be either:
    -absolute_or_relative_path_to_file_or_folder: exclude the corresponding element from analysis
    +absolute_or_relative_path_to_file_or_folder: include only the corresponding element for analysis
    --parameter=value
Options can be passed in any order.

----
/!\\ To be coherent with the Python import mecanism, and therefore to ensure a correct code analysis, even if only part of a project is intended to be analyzed, "project_folder" must still be the project root. For example, to analyze the folder "project_part" inside a project, from within the project folder, use (see EXCLUSION and INCLUSION PATHS below):
/!\\     python3 dep-grapy.py . +project_part [options...]
/!\\ instead of:
/!\\     python3 dep-grapy.py project_part [options...]
----

EXCLUSION and INCLUSION PATHS:
Relative paths are relative to the project folder. Absolute paths must point to a file or folder inside the project folder.
Folder and file exclusion or inclusion is performed using path-prefix matching. For example, if "please_exclude" is a folder just below the project folder, the option "-please_exclude" excludes the folder "please_exclude/some_subfolder", but not "some_subfolder/please_exclude/deeper_subfolder".
During project analysis, if the path of a folder or a Python file matches an excluded path (if any), it is discarded. Otherwise, if no inclusions have been specified, the folder/file is analyzed. If at least one inclusion has been specified, it is analyzed only if the path matches an included path.

OTHER OPTIONS:
The placeholders "parameter" and "value" in "--parameter=value" can be either:
    max_n_graphs: an integer. It limits the number of CCs being printed, shown or saved. Defaults: {MAX_N_GRAPHS}
    min_n_nodes:  an integer >= 2. Only CCs with at least this number of nodes are printed, shown or saved. Defaults: {MIN_N_NODES}
    max_n_nodes:  an integer >= min_n_nodes. Only CCs with at most this number of nodes are printed, shown or saved. Defaults: {MAX_N_NODES}
    sys_module:   y for yes, or n for n. Whether to include system modules and packages in the module graph (see below). Defaults: n
    sys_class:    equivalent of "sys_module" for classes (see below).
    print:        a string composed of characters among m (for module), c (for class), and f (for function/method). The specified graph(s) will be printed to console. Disable graph printing with an empty string. Defaults: empty string (i.e., --print=)
    show:         equivalent of "print" for graph display: The specified graph(s) will be shown in a Matplotlib [2] figure. Defaults: empty string (i.e., --show=)
    save:         equivalent of "print" for graph saving. Defaults: empty string (i.e., --save=)
    format:       the format to save the graph(s) in, among {_graph_formats}. Defaults: {DEFAULT_SAVING_FORMAT}
If a parameter is specified more than once, the last value is retained.

SYSTEM MODULES/CLASSES:
System modules: modules in absolute imports that can be imported with importlib.import_module and for which the returned module object:
    - either has no __file__ attribute (built-in module),
    - or has a __file__ attribute pointing into a folder of sys.path that is prefixed with sys.prefix or syst.exec_prefix.
(As a consequence of this rule, modules in relative imports are never considered as system modules.)
System classes: elements of dir(builtins) that are of type "type".

PRINTING/SHOWING/SAVING:
CCs with at least 2 nodes and which respect the conditions on "min_n_nodes" and "max_n_nodes" are called relevant CCs. Only the "max_n_graphs" relevant CCs with the largest number of nodes, or the largest number of edges at equal number of nodes, are printed, shown and/or saved.

PRINTING:
Whether printing is requested or not, import cycles (a.k.a. circular imports) are printed to console.

SAVING:
When using the "save" option, graphs are saved in the user's HOME folder with the name "title-G.index-now.nanoseconds" where:
    - title: an hardcoded name stating the type of graph (module, class, or function/method);
    - index: the index (using 1-based indexing) of the CC;
    - now: the current date and time (precision=seconds) with the format year-month-day_hours:minutes:seconds;
    - nanoseconds: the nanoseconds of the current time.
Graphs can also be saved through a Matplotlib [2] figure, either in {DEFAULT_SAVING_FORMAT} format [3] or in PNG format [4]. They are also placed in the user's HOME folder, but with the name "title-G.index-layout-now.nanoseconds" where:
    - title, index, now, and nanoseconds: see above;
    - layout: Capitalized version of the NetworkX [5] layout currently used, without the "{NETWORKX_LAYOUT_SUFFIX}" suffix.

EXAMPLE:
python3 dep-grapy.py project_folder -doc -resources +main.py +tools --sys_module=y --min_n_nodes=4 --show=mc

RELATED PROJECTS:
For advanced code analysis and dependency graphs building and exploration, see for example the Sourcetrail project [6].

REFERENCES:
[1] https://en.wikipedia.org/wiki/Component_(graph_theory)
[2] https://matplotlib.org/
[3] https://www.graphviz.org/doc/info/lang.html
[4] https://tools.ietf.org/html/rfc2083
[5] https://networkx.org
[6] https://www.sourcetrail.com/"""


def PrintUsage() -> None:
    """"""
    print(USAGE)
