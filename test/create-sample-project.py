# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2019)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import pathlib as phlb
import random as rndm
import sys as syst
from string import ascii_letters as ASCII_LETTERS

import matplotlib.pyplot as pypl
import networkx as ntkx

from brick.graph.output import DrawGraph, PrintGraph


# --- Parameters

BASE_FOLDER = "sample-project"  # Relative to folder of this file

N_MODULES_PER_FOLDERS = 3
N_FOLDERS_PER_FOLDERS = 2
PROJECT_DEPTH = 2

NEXT_AVAILABLE_MODULE = 1
NEXT_AVAILABLE_FOLDER = {1: "a", 2: "A"}

IMPORT_SCENARIOS = (
    "import {module}",
    "import {module} as {alias}",
    "from . import {module}",
    "from . import {module} as {alias}",
    "from .{module} import {element}",
    "from .{module} import {element} as {alias}",
    "from .. import {module}",
    "from .. import {module} as {alias}",
    "from ..{module} import {element}",
    "from ..{module} import {element} as {alias}",
)
ALIASES = ASCII_LETTERS

RANDOM_SEED = 0

SHOW_GRAPH = False
PRINT_GRAPH = True


# --- Checking and finalizing parameters

assert tuple(NEXT_AVAILABLE_FOLDER.keys()) == tuple(range(1, PROJECT_DEPTH + 1))

BASE_FOLDER = phlb.Path(__file__).parent / BASE_FOLDER
if BASE_FOLDER.exists():
    if not BASE_FOLDER.is_dir():
        print(f"{BASE_FOLDER}: Not a folder")
        syst.exit(-1)
else:
    BASE_FOLDER.mkdir(mode=0o700)

rndm.seed(a=RANDOM_SEED)


# --- Creating sample project skeleton

with open("test/copyright-header.txt", "r") as copyright_acc:
    copyright_header = copyright_acc.read() + "\n"

folders = [(BASE_FOLDER, 0)]
next_available_module = NEXT_AVAILABLE_MODULE
next_available_folder = dict(NEXT_AVAILABLE_FOLDER)  # Copy to leave original unchanged
modules = {}  # Folder-indexed dictionary of (name, full_name)-modules
all_modules_as_py = []
while folders.__len__() > 0:
    folder, depth = folders[0]

    for _ in range(N_MODULES_PER_FOLDERS):
        module = folder / f"module_{next_available_module}.py"
        next_available_module += 1
        with open(module, "w") as accessor:
            accessor.write(copyright_header)
        module_parts = module.relative_to(BASE_FOLDER).parts
        module_as_py = ".".join(module_parts)[:-3]
        module_short_long_names = (module_parts[-1][:-3], module_as_py)
        if folder in modules:
            modules[folder].append(module_short_long_names)
        else:
            modules[folder] = [module_short_long_names]
        all_modules_as_py.append(module_as_py)

    if depth < PROJECT_DEPTH:
        next_depth = depth + 1
        for _ in range(N_FOLDERS_PER_FOLDERS):
            subfolder = folder / f"package_{next_available_folder[next_depth]}"
            next_available_folder[next_depth] = chr(
                ord(next_available_folder[next_depth]) + 1
            )
            subfolder.mkdir(mode=0o700, exist_ok=True)
            folders.append((subfolder, next_depth))

    del folders[0]


# --- Adding import statements and building ground-truth import graph

folders = [(BASE_FOLDER, 0)]
next_available_module = NEXT_AVAILABLE_MODULE
next_available_folder = dict(NEXT_AVAILABLE_FOLDER)  # Copy to leave original unchanged
next_available_alias = 0
import_graph = ntkx.DiGraph()
while folders.__len__() > 0:
    folder, depth = folders[0]

    for _ in range(N_MODULES_PER_FOLDERS):
        short_name = f"module_{next_available_module}"
        next_available_module += 1
        module = folder / f"{short_name}.py"
        module_parts = module.relative_to(BASE_FOLDER).parts
        module_as_py = ".".join(module_parts)[:-3]
        # print(f"{short_name} in {folder}")
        with open(module, "a") as accessor:
            for scenario in IMPORT_SCENARIOS:
                if "." in scenario:
                    if ".." in scenario:
                        if depth < 2:
                            continue
                        available_modules = modules[folder.parent]
                    else:
                        if depth < 1:
                            continue
                        available_modules = modules[folder]
                    while True:
                        ishort_name, imodule_as_py = rndm.choice(available_modules)
                        imodule = ishort_name
                        if imodule != short_name:
                            break
                else:
                    while True:
                        imodule_as_py = rndm.choice(all_modules_as_py)
                        imodule = imodule_as_py
                        if (imodule != short_name) and not imodule.endswith("." + short_name):
                            break
                element = "something"
                alias = ALIASES[next_available_alias]
                next_available_alias = (next_available_alias + 1) % ALIASES.__len__()
                import_statement = scenario.format(
                    module=imodule, element=element, alias=alias
                )
                accessor.write(import_statement + "\n")
                import_graph.add_edge(module_as_py, imodule_as_py)
                # print("    " + import_statement)

    if depth < PROJECT_DEPTH:
        next_depth = depth + 1
        for _ in range(N_FOLDERS_PER_FOLDERS):
            subfolder = folder / f"package_{next_available_folder[next_depth]}"
            next_available_folder[next_depth] = chr(
                ord(next_available_folder[next_depth]) + 1
            )
            folders.append((subfolder, next_depth))

    del folders[0]


# --- Showing/Printing ground-truth import graph

if SHOW_GRAPH:
    figure = pypl.figure(constrained_layout=True)
    figure.set_constrained_layout(True)
    figure.set_constrained_layout_pads(wspace=0, hspace=0)
    axes = figure.add_subplot()
    DrawGraph(import_graph, 3, axes)
    pypl.show()

if PRINT_GRAPH:
    PrintGraph(import_graph)
