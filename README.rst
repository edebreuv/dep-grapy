===============================================
dep-grapy: Dependency Graphs of Python Projects
===============================================

What does ``dep-grapy`` do?
===========================

In Images
---------

.. |mod_img| image:: /documentation/module-graph.png
.. |fun_thn| image:: /documentation/function-graph-thumbnail.png
   :target: `fun_img`_
.. |mod_cpt| replace:: *Module graph of* ``dep-grapy`` *. Note the layout slider and the buttons for saving.*
.. |fun_cpt| replace:: *One of the 3 function graphs of* ``dep-grapy`` *(click to enlarge). Note the layout slider, the graph connected component slider, the node search box, and the buttons for saving.*
.. _fun_img: /documentation/function-graph.png

+-----------+-----------+
| |mod_img| | |fun_thn| |
+-----------+-----------+
| |mod_cpt| | |fun_cpt| |
+-----------+-----------+

.. note::
    Unfortunately, on the function graph, the planar layout of `NetworkX <https://networkx.org>`_ puts the nodes in clusters that are too compact to avoid superimposition.



In a Few Words
--------------

The ``dep-grapy`` project aims at building 3 types of dependency graphs of a Python project: ``module``, ``class``, and ``function/method call`` graphs. It relies on a static analysis of the project code. These graphs can be printed to console, shown in a `Matplotlib <https://matplotlib.org/>`_ figure and/or saved to disk in various graph formats or in `PNG <https://tools.ietf.org/html/rfc2083>`_ format. Import cycles (a.k.a. circular imports) are also printed to console.



Documentation
=============

The main (and only) runnable module is ``dep-graphy.py``. Run ``python3 dep-grapy.py`` (or simply ``python`` if it defaults to version 3 of the language) to get details on the options and behavior (output shown below).

.. parsed-literal::
    PURPOSE:
    Print, show, and/or save dependency graphs (module, class, function/method) of Python projects. These graphs might be composed of several connected components (CCs) [1] which are then shown/saved separately.

    USAGE:
    python3 dep-grapy.py project_folder [options...]
    where each option can be either:
        -absolute_or_relative_path_to_file_or_folder: exclude the corresponding element from analysis
        +absolute_or_relative_path_to_file_or_folder: include only the corresponding element for analysis
        --parameter=value
    Options can be passed in any order.

    ----
    /!\\ To be coherent with the Python import mecanism, and therefore to ensure a correct code analysis, even if only part of a project is intended to be analyzed, "project_folder" must still be the project root. For example, to analyze the folder "project_part" inside a project, from within the project folder, use (see EXCLUSION and INCLUSION PATHS below):
    /!\\     python3 dep-grapy.py . +project_part [options...]
    /!\\ instead of:
    /!\\     python3 dep-grapy.py project_part [options...]
    ----

    EXCLUSION and INCLUSION PATHS:
    Relative paths are relative to the project folder. Absolute paths must point to a file or folder inside the project folder.
    Folder and file exclusion or inclusion is performed using path-prefix matching. For example, if "please_exclude" is a folder just below the project folder, the option "-please_exclude" excludes the folder "please_exclude/some_subfolder", but not "some_subfolder/please_exclude/deeper_subfolder".
    During project analysis, if the path of a folder or a Python file matches an excluded path (if any), it is discarded. Otherwise, if no inclusions have been specified, the folder/file is analyzed. If at least one inclusion has been specified, it is analyzed only if the path matches an included path.

    OTHER OPTIONS:
    The placeholders "parameter" and "value" in "--parameter=value" can be either:
        max_n_graphs: an integer. It limits the number of CCs being printed, shown or saved. Defaults: {MAX_N_GRAPHS}
        min_n_nodes:  an integer >= 2. Only CCs with at least this number of nodes are printed, shown or saved. Defaults: {MIN_N_NODES}
        max_n_nodes:  an integer >= min_n_nodes. Only CCs with at most this number of nodes are printed, shown or saved. Defaults: {MAX_N_NODES}
        sys_module:   y for yes, or n for n. Whether to include system modules and packages in the module graph (see below). Defaults: n
        sys_class:    equivalent of "sys_module" for classes (see below).
        print:        a string composed of characters among m (for module), c (for class), and f (for function/method). The specified graph(s) will be printed to console. Disable graph printing with an empty string. Defaults: empty string (i.e., --print=)
        show:         equivalent of "print" for graph display: The specified graph(s) will be shown in a Matplotlib [2] figure. Defaults: empty string (i.e., --show=)
        save:         equivalent of "print" for graph saving. Defaults: empty string (i.e., --save=)
        format:       the format to save the graph(s) in, among {_graph_formats}. Defaults: {DEFAULT_SAVING_FORMAT}
    If a parameter is specified more than once, the last value is retained.

    SYSTEM MODULES/CLASSES:
    System modules: modules in absolute imports that can be imported with importlib.import_module and for which the returned module object:
        - either has no __file__ attribute (built-in module),
        - or has a __file__ attribute pointing into a folder of sys.path that is prefixed with sys.prefix or syst.exec_prefix.
    (As a consequence of this rule, modules in relative imports are never considered as system modules.)
    System classes: elements of dir(builtins) that are of type "type".

    PRINTING/SHOWING/SAVING:
    CCs with at least 2 nodes and which respect the conditions on "min_n_nodes" and "max_n_nodes" are called relevant CCs. Only the "max_n_graphs" relevant CCs with the largest number of nodes, or the largest number of edges at equal number of nodes, are printed, shown and/or saved.

    PRINTING:
    Whether printing is requested or not, import cycles (a.k.a. circular imports) are printed to console.

    SAVING:
    When using the "save" option, graphs are saved in the user's HOME folder with the name "title-G.index-now.nanoseconds" where:
        - title: an hardcoded name stating the type of graph (module, class, or function/method);
        - index: the index (using 1-based indexing) of the CC;
        - now: the current date and time (precision=seconds) with the format year-month-day_hours:minutes:seconds;
        - nanoseconds: the nanoseconds of the current time.
    Graphs can also be saved through a Matplotlib [2] figure, either in {DEFAULT_SAVING_FORMAT} format [3] or in PNG format [4]. They are also placed in the user's HOME folder, but with the name "title-G.index-layout-now.nanoseconds" where:
        - title, index, now, and nanoseconds: see above;
        - layout: Capitalized version of the NetworkX [5] layout currently used, without the "{NETWORKX_LAYOUT_SUFFIX}" suffix.

    EXAMPLE:
    python3 dep-grapy.py project_folder -doc -resources +main.py +tools --sys_module=y --min_n_nodes=4 --show=mc

    RELATED PROJECTS:
    For advanced code analysis and dependency graphs building and exploration, see for example the Sourcetrail project [6].

    REFERENCES:
    [1] https://en.wikipedia.org/wiki/Component_(graph_theory)
    [2] https://matplotlib.org/
    [3] https://www.graphviz.org/doc/info/lang.html
    [4] https://tools.ietf.org/html/rfc2083
    [5] https://networkx.org
    [6] https://www.sourcetrail.com/



Reliability of the Generated Graphs
===================================

General Accuracy
----------------

Since ``dep-grapy`` relies on a static analysis (of Python `Abstract Syntax Trees <https://docs.python.org/3/library/ast.html>`_), anything of dynamic nature will be overlooked. Moreover, only top-level elements are taken into account (see below for details).

Accuracy of ``module`` Graphs
-----------------------------

All modules and packages imported with ``import`` statements, or *partially imported* with ``from ... import`` statements, declared at zero-indentation are supposed to be included in the dependency graph. Everything else is ignored (for example, ``import`` statements local to an ``if __name__ == "__main__"`` section) or missed (for example, modules imported using the `importlib package <https://docs.python.org/3/library/importlib.html>`_). System modules and packages can be included or not.

Accuracy of ``class`` Graphs
----------------------------

All the links between top-level classes defined statically and their base classes are supposed to be included in the dependency graph. Everything else is ignored (nested classes) or missed (classes defined programmatically). Standard Python classes can be included or not.

Accuracy of ``function/method call`` Graphs
-------------------------------------------

Most of the time, these graphs will be (largely) incomplete, and they can contain mistakes (for example, due to homonymous functions). Their building relies on naive rules. Still, they can be used as a starting point to build correct ``call`` graphs manually, using tools such as `QVGE <https://github.com/ArsMasiuk/qvge>`_. To limit the size of ``call`` graphs, functions and methods identified as standard in Python are ignored (for example, the ``sorted`` function or the ``sort`` method of the ``list`` class).



Thanks
======

The project is developed with `PyCharm Community <https://www.jetbrains.com/pycharm>`_.

The code is formatted by `Black <https://github.com/psf/black>`_, *The Uncompromising Code Formatter*.

The imports are ordered by `isort <https://github.com/timothycrosley/isort>`_... *your imports, so you don't have to*.
