# Copyright CNRS/Inria/UCA
# Contributor(s): Eric Debreuve (since 2019)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import sys as syst
from typing import Dict, Optional, Sequence, Tuple

from matplotlib.pyplot import show as ShowFigures

from brick.app.option import OptionsFromArguments, ProjectFolderAndFilesFromArguments
from brick.figure import graph_figure_t
from brick.graph.building import (
    DependencyGraphs,
    RelevantConnectedComponents,
    dep_graph_t,
)
from brick.graph.output import GraphName, PrintGraph, PrintGraphCycles, SaveGraph
from documentation.usage import PrintUsage


# TODO: long-term: check astroid
# TODO: apparently, dependency cycles are not always printed as mentioned in the doc, only if printing something


def DepGrapyAnalysis(arguments: Sequence[str]) -> Tuple[int, Optional[Sequence[str]]]:
    """"""
    project_folder, project_files, issue = ProjectFolderAndFilesFromArguments(arguments)
    if project_folder is None:
        if issue is not None:
            issue = (issue,)
        return -1, issue

    *options, issues = OptionsFromArguments(arguments, project_folder)
    if issues.__len__() > 0:
        return -2, issues

    (
        include_list,
        exclude_list,
        include_sys_module,
        include_sys_class,
        *limits,
        printing_list,
        showing_list,
        saving_list,
        saving_format,
    ) = options

    (max_n_graphs, min_n_nodes, max_n_nodes) = limits
    # In case OptionsFromArguments stops checking this lower bound one day
    min_n_nodes = max(min_n_nodes, 2)

    graphs = DependencyGraphs(
        project_folder,
        project_files,
        tuple(set(printing_list).union(showing_list).union(saving_list)),
        include_list=include_list,
        exclude_list=exclude_list,
        include_sys_module=include_sys_module,
        include_sys_class=include_sys_class,
    )

    # Print and save first so that saving is not delayed by blocking windows when showing
    _PrintGraphs(graphs, printing_list, max_n_graphs, min_n_nodes, max_n_nodes)
    for name, graph in graphs.items():
        if graph is not None:
            PrintGraphCycles(graph, name)
    _SaveGraphs(
        graphs, saving_list, max_n_graphs, min_n_nodes, max_n_nodes, saving_format
    )
    if showing_list.__len__() > 0:
        _ = _DepGrapyFigures(
            graphs, showing_list, max_n_graphs, min_n_nodes, max_n_nodes
        )
        ShowFigures(block=True)

    return 0, None


def _PrintGraphs(
    graphs: Dict[str, dep_graph_t],
    names: Sequence[str],
    max_n_graphs: int,
    min_n_nodes: int,
    max_n_nodes: int,
) -> None:
    """"""
    for name in names:
        relevant_conn_cmps, n_relevant_conn_cmps = _RelevantConnectedComponents(
            graphs[name], name, max_n_graphs, min_n_nodes, max_n_nodes
        )
        if n_relevant_conn_cmps > 0:
            print(f"--- Graph {name}:")
            for graph in relevant_conn_cmps:
                PrintGraph(graph[0])


def _SaveGraphs(
    graphs: Dict[str, dep_graph_t],
    names: Sequence[str],
    max_n_graphs: int,
    min_n_nodes: int,
    max_n_nodes: int,
    saving_format: str,
) -> None:
    """"""
    for name in names:
        relevant_conn_cmps, _ = _RelevantConnectedComponents(
            graphs[name], name, max_n_graphs, min_n_nodes, max_n_nodes
        )
        for g_idx, graph in enumerate(relevant_conn_cmps):
            SaveGraph(graph[0], GraphName(name, g_idx), saving_format)


def _DepGrapyFigures(
    graphs: Dict[str, dep_graph_t],
    names: Sequence[str],
    max_n_graphs: int,
    min_n_nodes: int,
    max_n_nodes: int,
) -> Sequence[graph_figure_t]:
    """"""
    output = []

    for name in names:
        relevant_conn_cmps, n_relevant_conn_cmps = _RelevantConnectedComponents(
            graphs[name], name, max_n_graphs, min_n_nodes, max_n_nodes
        )
        if n_relevant_conn_cmps > 0:
            figure = graph_figure_t.NewForConnectedComponents(relevant_conn_cmps, name)
            output.append(figure)

    return output


def _RelevantConnectedComponents(
    graph: dep_graph_t, name: str, max_n_graphs: int, min_n_nodes: int, max_n_nodes: int
) -> Tuple[Sequence[Tuple[dep_graph_t, int]], int]:
    """"""
    relevant_conn_cmps, n_conn_cmps = RelevantConnectedComponents(
        graph, min_n_nodes, max_n_nodes
    )
    n_relevant_conn_cmps = relevant_conn_cmps.__len__()

    if n_relevant_conn_cmps == 0:
        print(
            f"{name}: Graph has no connected component "
            f"with at least {min_n_nodes} and at most {max_n_nodes} nodes"
        )
    elif n_relevant_conn_cmps < min(n_conn_cmps, max_n_graphs):
        print(
            f"{name}: Graph has only {n_relevant_conn_cmps} connected component(s) "
            f"out of {n_conn_cmps} in total/at most {max_n_graphs} requested "
            f"with at least {min_n_nodes} and at most {max_n_nodes} nodes"
        )

    if n_relevant_conn_cmps > max_n_graphs:
        relevant_conn_cmps = relevant_conn_cmps[-max_n_graphs:]
        n_relevant_conn_cmps = max_n_graphs

    return relevant_conn_cmps, n_relevant_conn_cmps


if __name__ == "__main__":
    #
    exit_code, issues_ = DepGrapyAnalysis(syst.argv)
    if exit_code != 0:
        if issues_ is not None:
            print("\n".join(issues_) + "\n")
        PrintUsage()
    syst.exit(exit_code)

    # If graphs are saved, then add the following instructions at the top of the curly braces section:
    #     rankdir=LR;
    #     node[shape=Mrecord, color=Blue, fontcolor=Blue];
    # Then run: dot -Tpng XXX-graph-YYY.dot > XXX-graph-YYY.png
