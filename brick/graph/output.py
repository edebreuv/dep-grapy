# Copyright CNRS/Inria/UCA
# Contributor(s): Eric Debreuve (since 2019)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import networkx as ntkx
from matplotlib.axes import Axes as axes_t

from brick.app.option import GRAPH_FORMATS
from brick.graph.building import dep_graph_t
from brick.path import SavePathWithoutExtension


NODE_LAYOUTS = (
    ntkx.circular_layout,
    ntkx.kamada_kawai_layout,
    ntkx.planar_layout,
    ntkx.shell_layout,
    ntkx.spring_layout,
    ntkx.spectral_layout,
)
NODE_LABEL_STYLE = {"boxstyle": "round", "ec": "k", "fc": "white", "alpha": 0.7}


def PrintGraph(graph: dep_graph_t, /) -> None:
    """"""
    for src, tgt in graph.edges:
        print(f"{src} -> {tgt}")


def PrintGraphCycles(graph: dep_graph_t, name: str, /) -> None:
    """
    Draw with:
    figure = pypl.figure(constrained_layout=True)
    grid = figure.add_gridspec(ncols=cycles.__len__())
    for c_idx, cycle in enumerate(cycles):
        axes = figure.add_subplot(grid[0, c_idx])
        DrawGraph(graph.subgraph(cycle), layout_idx, axes)

    # figure.set_constrained_layout(True)
    figure.set_constrained_layout_pads(wspace=0, hspace=0)
    """
    cycles = tuple(ntkx.simple_cycles(graph))
    true_cycles = tuple(filter(lambda _ccl: _ccl.__len__() > 1, cycles))

    if true_cycles.__len__() > 0:
        print(f"--- Cycle(s) of Graph {name}:")
        for cycle in true_cycles:
            as_str = map(str, cycle)
            print(" ->\n    ".join(as_str))


def DrawGraph(graph: dep_graph_t, layout_idx: int, axes: axes_t, /) -> None:
    """"""
    positions = NODE_LAYOUTS[layout_idx](graph)
    labels = {_nde: _LabelAsNameNLContext(_nde) for _nde in graph.nodes}

    ntkx.draw_networkx_edges(graph, positions, ax=axes, edge_color="b", arrowsize=20)
    ntkx.draw_networkx_nodes(graph, positions, ax=axes, node_color="g", alpha=0.5)
    ntkx.draw_networkx_labels(
        graph,
        positions,
        labels=labels,
        ax=axes,
        font_size=9,
        verticalalignment="bottom",
        bbox=NODE_LABEL_STYLE,
    )


def _LabelAsNameNLContext(label: str, /) -> str:
    """"""
    idx = label.rfind(".")
    if idx == -1:
        return label

    return f"{label[(idx+1):]}\n{label[:idx]}"


def SaveGraph(graph: dep_graph_t, name: str, saving_format: str, /) -> None:
    """"""
    path = str(SavePathWithoutExtension(name))
    extension, _SaveGraph = GRAPH_FORMATS[saving_format]

    _SaveGraph(graph, f"{path}.{extension}")


def GraphName(title: str, index: int, /) -> str:
    """"""
    return f"{title}-G.{index + 1}"
