# Copyright CNRS/Inria/UCA
# Contributor(s): Eric Debreuve (since 2019)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import ast as astr
from ast import parse as ASTTreeOfCode
from typing import Any, Dict, Optional, Sequence, Tuple

import networkx as ntkx

from brick.app.option import GRAPH_NAMES
from brick.module import ShouldAccountForModule
from brick.node.function import FullNameOfFunction, FunctionFromNode, function_h
from brick.node.classes import (
    AncestorsOfClass,
    MethodsOfClass,
    UpdateFunctionsWithMethods,
)
from brick.path import PathToPythonPath, path_t


dep_graph_t = ntkx.DiGraph


def DependencyGraphs(
    project_folder: path_t,
    project_files: Sequence[path_t],
    names: Sequence[str],
    /,
    *,
    include_list: Optional[Sequence[path_t]] = None,
    exclude_list: Optional[Sequence[path_t]] = None,
    include_sys_module: bool = False,
    include_sys_class: bool = False,
) -> Dict[str, Optional[dep_graph_t]]:
    """"""
    graphs = {_nme: None for _nme in GRAPH_NAMES.values()}
    for name in names:
        graphs[name] = dep_graph_t()

    code_elements = {}
    for python_file in project_files:
        if _IsExcludedOrNotIncluded(python_file, include_list, exclude_list):
            continue

        relative_path = python_file.relative_to(project_folder)
        namespace = PathToPythonPath(relative_path)
        with open(python_file, "r") as accessor:
            ast_tree = ASTTreeOfCode(accessor.read())

        elements = _CodeAnalysis(
            namespace,
            ast_tree,
            graphs["module-graph"],
            graphs["class-graph"],
            include_sys_module=include_sys_module,
            include_sys_class=include_sys_class,
        )
        code_elements[namespace] = elements

    if graphs["function-graph"] is not None:
        graph = graphs["function-graph"]

        classes = {}
        for _, _, _, local_classes in code_elements.values():
            classes.update(local_classes)

        functions = {}
        for namespace, (
            local_modules,
            local_elements,
            local_functions,
            local_classes,
        ) in code_elements.items():
            fct_w_methods = list(local_functions)
            UpdateFunctionsWithMethods(fct_w_methods, local_classes, classes)
            functions[namespace] = (local_modules, local_elements, fct_w_methods)
        del code_elements
        for namespace, (_, _, local_functions) in functions.items():
            for function in local_functions:
                for node in function["call_nodes"]:
                    full_name = FullNameOfFunction(
                        node, function["name"], namespace, functions
                    )
                    if full_name is not None:
                        graph.add_edge(f"{namespace}.{function['name']}", full_name)

    # Despite exclusion from parsing of the requested modules, elements in these modules might be referenced by others.
    # Hence, it is necessary to remove these references. (This could also be done while parsing though.)
    if exclude_list is not None:
        exclude_list = (_pth.relative_to(project_folder) for _pth in exclude_list)
        exclude_list = tuple(PathToPythonPath(_pth) for _pth in exclude_list)

        for graph in graphs.values():
            if graph is not None:
                _RemoveUnwantedNodes(graph, exclude_list)

    return graphs


def _IsExcludedOrNotIncluded(
    path: path_t,
    include_list: Optional[Sequence[path_t]],
    exclude_list: Optional[Sequence[path_t]],
    /,
) -> bool:
    """"""
    if exclude_list is not None:
        for excluded in exclude_list:
            if path.is_relative_to(excluded):
                # First, exclude if found in exclusion list
                return True

    if include_list is not None:
        for included in include_list:
            if path.is_relative_to(included):
                # Second, include if found in inclusion list
                return False

    # Finally, include only if there is no inclusion list (meaning that everything is implicitly included)
    return include_list is not None


def _CodeAnalysis(
    namespace: str,
    ast_tree: astr.AST,
    module_dep_graph: dep_graph_t,
    class_dep_graph: dep_graph_t,
    include_sys_module: bool = False,
    include_sys_class: bool = False,
) -> Tuple[
    Sequence[Tuple[str, str]],
    Sequence[Tuple[str, str, str]],
    Sequence[function_h],
    Dict[str, Dict[str, Sequence[Any]]],
]:
    """"""
    imported_modules = []
    imported_elements = []
    functions = []
    classes = {}

    class_nodes = []
    for node in astr.walk(ast_tree):
        if getattr(node, "col_offset", 1) > 0:
            # Only deal with top-level elements (note that not all node types have a col_offset)
            pass
        elif isinstance(node, astr.Import):
            for module_names in node.names:
                if ShouldAccountForModule(
                    module_names.name, 0, namespace, include_sys_module
                ):
                    imported_modules.append((module_names.asname, module_names.name))
        elif isinstance(node, astr.ImportFrom):
            if ShouldAccountForModule(node.module, node.level, namespace, include_sys_module):
                if node.module is None:  # from .+ import...
                    if (node.level == 1) and ("." not in namespace):
                        # At the project root (no dots) and importing there (level=1)
                        module_basename = ""
                    elif namespace.count(".") < node.level:
                        print(f"In {namespace}: Trying to relatively import a module outside the project folder")
                        continue
                    else:
                        module_basename = namespace.rsplit(".", node.level)[0] + "."
                elif node.level > 0:  # from .+module import...
                    if (node.level == 1) and ("." not in namespace):
                        # At the project root (no dots) and importing there (level=1)
                        module_basename = node.module
                    elif namespace.count(".") < node.level:
                        print(f"In {namespace}: Trying to relatively import a module outside the project folder")
                        continue
                    else:
                        module_basename = namespace.rsplit(".", node.level)[0] + "." + node.module
                else:  # Absolute import
                    module_basename = node.module
                for element_names in node.names:
                    name = element_names.asname or element_names.name
                    if node.module is None:
                        module_name = f"{module_basename}{element_names.name}"
                    else:
                        module_name = module_basename
                    imported_elements.append((module_name, name, element_names.name))
        elif isinstance(node, astr.ClassDef):
            class_nodes.append(node)
        elif isinstance(node, astr.FunctionDef):
            functions.append(FunctionFromNode(node))

    if module_dep_graph is not None:
        for _, as_defined in imported_modules:
            module_dep_graph.add_edge(namespace, as_defined)
        for from_module, _, _ in imported_elements:
            module_dep_graph.add_edge(namespace, from_module)

    class_names = tuple(_nde.name for _nde in class_nodes)
    for node in class_nodes:
        full_name = f"{namespace}.{node.name}"
        ancestors = AncestorsOfClass(
            node,
            namespace,
            class_names,
            imported_modules,
            imported_elements,
            include_sys_class,
        )
        methods = MethodsOfClass(node)
        classes[full_name] = {"ancestors": ancestors, "methods": methods}

        if class_dep_graph is not None:
            for ancestor in ancestors:
                class_dep_graph.add_edge(full_name, ancestor)

    return imported_modules, imported_elements, functions, classes


def _RemoveUnwantedNodes(graph: dep_graph_t, unwanted_nodes: Sequence[str]) -> None:
    """"""
    to_be_removed = tuple(
        _nde for _nde in graph if any(_nde.startswith(_exc) for _exc in unwanted_nodes)
    )
    if to_be_removed.__len__() > 0:
        graph.remove_nodes_from(to_be_removed)
    graph.remove_nodes_from(list(ntkx.isolates(graph)))


def RelevantConnectedComponents(
    graph: dep_graph_t, min_n_nodes: int, max_n_nodes: int
) -> Tuple[Sequence[Tuple[dep_graph_t, int]], int]:
    """"""
    conn_cmps = tuple(ntkx.weakly_connected_components(graph))
    n_conn_cmps = conn_cmps.__len__()
    conn_cmp_lengths = tuple(map(len, conn_cmps))

    relevant_conn_cmps = sorted(
        (
            (graph.subgraph(_sub), _lgt)
            for _sub, _lgt in zip(conn_cmps, conn_cmp_lengths)
            if min_n_nodes <= _sub.__len__() <= max_n_nodes
        ),
        key=lambda _elm: _elm[1] + (1.0 / (1.0 + (1.0 / _elm[0].edges.__len__()))),
    )

    return relevant_conn_cmps, n_conn_cmps
