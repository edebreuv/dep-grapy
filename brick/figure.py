# Copyright CNRS/Inria/UCA
# Contributor(s): Eric Debreuve (since 2019)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from __future__ import annotations

from typing import Any, Callable, Optional, Sequence, Tuple

import matplotlib.pyplot as pypl
from matplotlib.widgets import Button as button_t
from matplotlib.widgets import Slider as slider_t
from matplotlib.widgets import TextBox as text_t

from brick.app.defaults import DEFAULT_SAVING_FORMAT
from brick.graph.building import dep_graph_t
from brick.graph.output import NODE_LAYOUTS, DrawGraph, GraphName, SaveGraph
from brick.path import SavePathWithoutExtension


SLIDER_HEIGHT_RATIO = 0.03
SEARCH_HEIGHT_RATIO = 0.075
BUTTON_HEIGHT_RATIO = 0.1

MIN_SEARCH_PATTERN_LENGTH = 3
TEXT_FOUND_MARKER = "*"

NETWORKX_LAYOUT_SUFFIX = "_layout"
_LAYOUT_SUFFIX_LENGTH = NETWORKX_LAYOUT_SUFFIX.__len__()

_SLIDER_STYLE = {
    "axis": "x",
    "direction": "in",
    "bottom": True,
    "top": True,
    "color": "r",
    "labelsize": 8,
    "labelrotation": 45,
}


class graph_figure_t:

    __slots__ = (
        "title",
        "graphs",
        "graph_nodes",
        "previous_search",
        "figure",
        "grid",
        "axes",
        "layout_slider",
        "graph_slider",
        "search_text",
        "save_buttons",
    )

    title: str
    graphs: Sequence[dep_graph_t]
    graph_nodes: Sequence[Sequence[str]]  # List of nodes for each graphs
    previous_search: str
    figure: pypl.Figure
    grid: Any
    axes: pypl.Axes
    layout_slider: slider_t
    graph_slider: Optional[slider_t]
    search_text: Optional[text_t]
    save_buttons: Sequence[button_t]

    def __init__(self):
        """"""
        for slot in graph_figure_t.__slots__:
            setattr(self, slot, None)

    @classmethod
    def NewForConnectedComponents(
        cls, graph_infos: Sequence[Tuple[dep_graph_t, int]], name: str, /
    ) -> graph_figure_t:
        """"""
        instance = cls()

        graphs = tuple(_nfo[0] for _nfo in graph_infos)
        graph_lengths = tuple(_nfo[1] for _nfo in graph_infos)
        n_graphs = graphs.__len__()
        more_than_1 = int(n_graphs > 1)

        axes_height_ratio = (
            1.0
            - (more_than_1 + 1) * SLIDER_HEIGHT_RATIO
            - more_than_1 * SEARCH_HEIGHT_RATIO
            - BUTTON_HEIGHT_RATIO
        )
        figure = pypl.figure(constrained_layout=True)
        grid = figure.add_gridspec(
            nrows=3 + 2 * more_than_1,
            ncols=2,
            height_ratios=(axes_height_ratio,)
            + (more_than_1 + 1) * (SLIDER_HEIGHT_RATIO,)
            + more_than_1 * (SEARCH_HEIGHT_RATIO,)
            + (BUTTON_HEIGHT_RATIO,),
        )
        axes = figure.add_subplot(grid[0, :], label="main")

        # figure.set_constrained_layout(True)
        figure.set_constrained_layout_pads(wspace=0, hspace=0)

        instance.title = name.translate({ord("-"): " ", ord("_"): " "}).title()
        instance.graphs = graphs
        instance.graph_nodes = tuple(tuple(_gph.nodes) for _gph in graphs)
        instance.figure = figure
        instance.grid = grid
        instance.axes = axes
        instance.layout_slider = instance._LayoutSlider()
        instance.graph_slider = instance._GraphSlider(n_graphs, graph_lengths)
        instance.search_text = instance._SearchText(n_graphs)
        instance.save_buttons = instance._SaveButtons()

        instance.UpdateLayoutAndGraph(0, 0)

        return instance

    def _LayoutSlider(self) -> slider_t:
        #
        tick_labels = ("",) + tuple(_fct.__name__[:-7] for _fct in NODE_LAYOUTS)

        return _Slider(
            self.figure,
            self.grid[1, :],
            "layout_slider",
            "Layout",
            NODE_LAYOUTS.__len__(),
            tick_labels,
            self.UpdateLayout,
        )

    def _GraphSlider(
        self, n_graphs: int, graph_lengths: Tuple[int, ...], /
    ) -> Optional[slider_t]:
        #
        if n_graphs == 1:
            return None

        tick_positions = range(n_graphs + 1)
        tick_labels = [
            f"{_pos}[{_lgt}]"
            for _pos, _lgt in zip(tick_positions, ("",) + graph_lengths)
        ]
        tick_labels[0] = ""

        return _Slider(
            self.figure,
            self.grid[2, :],
            "graph_slider",
            "Graph",
            n_graphs,
            tick_labels,
            self.UpdateGraph,
        )

    def _SearchText(self, n_graphs: int, /) -> Optional[text_t]:
        """"""
        if n_graphs == 1:
            return None

        button_room = self.figure.add_subplot(
            self.grid[-2, :], label="search_text_room"
        )
        button = text_t(
            button_room, f"Search Graph Nodes (>={MIN_SEARCH_PATTERN_LENGTH})"
        )
        button.on_submit(self.SearchNodes)

        return button

    def _SaveButtons(self) -> Sequence[button_t, button_t]:
        """"""
        output = []

        for idx, (title, Action) in enumerate(
            zip(("Save FIG", "Save Graph"), (self.SaveFigure, self.SaveGraph))
        ):
            button_room = self.figure.add_subplot(self.grid[-1, idx], label=title)
            button = button_t(button_room, title)
            button.on_clicked(Action)
            output.append(button)

        return output

    def UpdateLayout(self, layout_idx: float, /) -> None:
        """"""
        layout_idx = int(layout_idx)
        if layout_idx == 0:
            self.layout_slider.set_val(1)
            layout_idx = 1
        if self.graph_slider is None:
            graph_idx = 1
        else:
            graph_idx = int(self.graph_slider.val)
        self.UpdateLayoutAndGraph(layout_idx - 1, graph_idx - 1)

    def UpdateGraph(self, graph_idx: float, /) -> None:
        """"""
        graph_idx = int(graph_idx)
        if graph_idx == 0:
            self.graph_slider.set_val(1)
            graph_idx = 1
        self.UpdateLayoutAndGraph(int(self.layout_slider.val) - 1, graph_idx - 1)

    def UpdateLayoutAndGraph(self, layout_idx: int, graph_idx: int, /) -> None:
        """"""
        graph = self.graphs[graph_idx]

        self.axes.clear()
        try:
            DrawGraph(graph, layout_idx, self.axes)
        except Exception as exc:
            self.axes.text(
                0.5,
                0.5,
                "Error: " + str(exc),
                horizontalalignment="center",
                verticalalignment="center",
                color="red",
            )
        self.axes.set_title(self.title)

    def SearchNodes(self, searched_node: str) -> None:
        """"""
        searched_node = searched_node.strip()
        if (searched_node.__len__() < MIN_SEARCH_PATTERN_LENGTH) or (
            searched_node == self.previous_search
        ):
            return
        self.previous_search = searched_node

        xtick_labels = self.graph_slider.ax.get_xticklabels()
        for g_idx, nodes in enumerate(self.graph_nodes):
            text = xtick_labels[g_idx + 1].get_text()
            if text.startswith(TEXT_FOUND_MARKER):
                text = text[1:]

            for node in nodes:
                if searched_node in node:
                    text = TEXT_FOUND_MARKER + text
                    break

            xtick_labels[g_idx + 1].set_text(text)

        self.graph_slider.ax.set_xticklabels(xtick_labels)
        self.figure.canvas.draw_idle()

    def SaveFigure(self, _) -> None:
        """"""
        layout_idx = int(self.layout_slider.val - 1)
        if self.graph_slider is None:
            graph_idx = 0
        else:
            graph_idx = int(self.graph_slider.val - 1)
        node_layout = NODE_LAYOUTS[layout_idx].__name__[:-_LAYOUT_SUFFIX_LENGTH].title()
        path = SavePathWithoutExtension(
            f"{GraphName(self.title, graph_idx)}-{node_layout}"
        )
        self.figure.savefig(str(path) + ".png")

    def SaveGraph(self, _) -> None:
        """"""
        if self.graph_slider is None:
            graph_idx = 0
        else:
            graph_idx = int(self.graph_slider.val - 1)
        graph = self.graphs[graph_idx]
        SaveGraph(graph, GraphName(self.title, graph_idx), DEFAULT_SAVING_FORMAT)


def _Slider(
    figure: pypl.Figure,
    axes: pypl.SubplotSpec,
    mpl_label: str,
    title: str,
    max_value: int,
    tick_labels: Sequence[str],
    Action: Callable[[int], None],
    /,
) -> slider_t:
    #
    slider_room = figure.add_subplot(axes, label=mpl_label)

    output = slider_t(
        slider_room,
        title,
        0,
        max_value,
        valinit=1.0,
        valstep=1.0,
        valfmt="%d",
    )
    output.on_changed(Action)

    # The code below is a workaround for the ticks and labels not showing anymore
    slider_room.set_axis_on()
    slider_room.set_facecolor("w")
    slider_room.yaxis.set_visible(False)
    slider_room.spines["left"].set_visible(False)
    slider_room.spines["right"].set_visible(False)
    slider_room.spines["top"].set_visible(False)
    slider_room.spines["bottom"].set_visible(False)

    tick_positions = tuple(range(max_value + 1))
    slider_room.xaxis.set_visible(True)
    slider_room.set_xticks(tick_positions)
    slider_room.set_xticklabels(tick_labels)
    slider_room.tick_params(**_SLIDER_STYLE)

    return output
