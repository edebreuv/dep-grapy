# Copyright CNRS/Inria/UCA
# Contributor(s): Eric Debreuve (since 2019)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import importlib as imlb
from typing import Optional, Sequence, Tuple

from brick.system import INSTALLED_PACKAGE_FOLDER, path_t


# Tuple of (alias, module).
# Intended to be the values of a dictionary keyed by the importer module.
imported_modules_h = Sequence[Tuple[Optional[str], str]]

# Tuple of (name or alias, full name=module.name, module).
# Intended to be the values of a dictionary keyed by the importer module.
imported_elements_h = Sequence[Tuple[str, str, str]]


def ShouldAccountForModule(
    module: str, level: int, from_module: str, include_system: bool, /
) -> bool:
    """"""
    if include_system or (level > 0):  # >0=>relative import
        output = True
    else:
        # TODO: find a better way to identify standard modules, if possible
        try:
            module = imlb.import_module(module)
            if hasattr(module, "__file__"):
                path = path_t(module.__file__)
                output = not any(
                    path.is_relative_to(_pth) for _pth in INSTALLED_PACKAGE_FOLDER
                )
            else:
                output = False  # This is a built-in module
        except ImportError:
            output = True
        except Exception as exception:
            print(
                f"{module}: Imported from {from_module}: Cannot be checked if standard or not:\n{exception}"
            )
            output = True

    return output
