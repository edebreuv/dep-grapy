# Copyright CNRS/Inria/UCA
# Contributor(s): Eric Debreuve (since 2019)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import ast as astr
from typing import Any, Dict, List, Optional, Sequence

from brick.module import imported_elements_h, imported_modules_h
from brick.node.generic import ASTNodeToStr, NameAndContextOfAttribute
from brick.node.function import MethodFromNode, function_h, method_h
from brick.system import BUILTIN_TYPES


def AncestorsOfClass(
    node: astr.ClassDef,
    namespace: str,
    class_names: Sequence[str],
    imported_modules: imported_modules_h,
    imported_elements: imported_elements_h,
    include_system: bool,
    /,
) -> Sequence[str]:
    """"""
    output = []

    for ancestor_node in node.bases:
        full_name = _FullNameOfAncestor(
            ancestor_node,
            node.name,
            namespace,
            class_names,
            imported_modules,
            imported_elements,
            include_system,
        )
        if full_name is not None:
            output.append(full_name)

    return output


def _FullNameOfAncestor(
    ancestor_node: astr.AST,
    parent_name: str,
    namespace: str,
    class_names: Sequence[str],
    imported_modules: imported_modules_h,
    imported_elements: imported_elements_h,
    include_system: bool,
    /,
) -> Optional[str]:
    """"""
    if isinstance(ancestor_node, astr.Name):
        name = ancestor_node.id
        if name.startswith("__"):
            return None

        # Look for ancestor among standard Python types
        if name in BUILTIN_TYPES:
            if include_system:
                return name
            else:
                return None
        # Look for ancestor in same module
        if name in class_names:
            return f"{namespace}.{name}"
        # Look for ancestor in imported elements
        for from_module, as_used, as_defined in imported_elements:
            if as_used == name:
                return f"{from_module}.{as_defined}"
    elif isinstance(ancestor_node, astr.Attribute):
        name, context = NameAndContextOfAttribute(ancestor_node)
        if (name is None) or name.startswith("__"):
            return None

        # Look for context in imported modules
        for as_used, as_defined in imported_modules:
            if (context == as_used) or (context == as_defined):
                return f"{as_defined}.{name}"
    else:
        print(
            f"{type(ancestor_node).__name__}: "
            f"Unimplemented ancestor type with attributes: "
            f"{ASTNodeToStr(ancestor_node)}"
        )
        return None

    print(f"{name}: Unfound ancestor of {namespace}.{parent_name}")

    return None


def MethodsOfClass(node: astr.ClassDef, /) -> Sequence[method_h]:
    """"""
    output = []

    for body_node in node.body:
        if isinstance(body_node, astr.FunctionDef) and not body_node.name.startswith(
            "__"
        ):
            output.append(MethodFromNode(body_node, node.name))

    return output


def UpdateFunctionsWithMethods(
    functions: List[function_h],
    local_classes: Dict[str, Dict[str, Sequence[Any]]],
    classes: Dict[str, Dict[str, Sequence[Any]]],
) -> None:
    """"""
    for class_ in local_classes.values():
        functions.extend(class_["methods"])
        ancestors = {}
        for ancestor in class_["ancestors"]:
            if ancestor in classes:
                # Reason for not being in classes: it is defined in an excluded file
                ancestors[ancestor] = classes[ancestor]
        UpdateFunctionsWithMethods(functions, ancestors, classes)
