# Copyright CNRS/Inria/UCA
# Contributor(s): Eric Debreuve (since 2019)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import ast as astr
from typing import Any, Dict, Optional, Sequence, Tuple, Union

from brick.node.generic import ASTNodeToStr, NameAndContextOfAttribute
from brick.system import BUILTIN_FUNCTIONS, BUILTIN_METHODS, BUILTIN_TYPES


function_h = Dict[str, Any]
method_h = Dict[str, Any]


def FunctionFromNode(node: astr.FunctionDef) -> function_h:
    """"""
    output = {"name": node.name}

    call_nodes = []
    for body_node in node.body:
        for subnode in astr.walk(body_node):
            if isinstance(subnode, astr.Call):
                call_nodes.append(subnode.func)
    output["call_nodes"] = call_nodes

    return output


def MethodFromNode(node: astr.FunctionDef, defined_by: str) -> method_h:
    """"""
    output = FunctionFromNode(node)
    output["defined_by"] = defined_by

    return output


def FunctionIsMethod(function: Union[function_h, method_h]) -> bool:
    """"""
    return "defined_by" in function


def FullNameOfFunction(
    node: astr.AST,
    caller_name: str,
    namespace: str,
    functions: Dict[
        str,
        Tuple[
            Sequence[Tuple[str, str]],
            Sequence[Tuple[str, str, str]],
            Sequence[function_h],
        ],
    ],
) -> Optional[str]:
    """"""
    imported_modules = functions[namespace][0]
    imported_elements = functions[namespace][1]

    if isinstance(node, astr.Name):
        name = node.id
        if name.startswith("__"):
            return None

        # Look for function among standard Python functions and types (when called as constructors)
        if name in BUILTIN_FUNCTIONS + BUILTIN_TYPES:
            return None
        # Look for function in same module
        function_names = (
            _fct["name"]
            for _fct in functions[namespace][2]
            if not FunctionIsMethod(_fct)
        )
        if name in function_names:
            return f"{namespace}.{name}"
        # Look for function in imported elements
        for from_module, as_used, as_defined in imported_elements:
            if as_used == name:
                return f"{from_module}.{as_defined}"
    elif isinstance(node, astr.Attribute):
        name, context = NameAndContextOfAttribute(node)
        if (name is None) or name.startswith("__"):
            return None

        # Look for method in same module
        for function in functions[namespace][2]:
            if FunctionIsMethod(function) and (function["name"] == name):
                return f"{namespace}.{function['defined_by']}.{name}"
        # Look for context in imported modules
        for as_used, as_defined in imported_modules:
            if (context == as_used) or (context == as_defined):
                return f"{as_defined}.{name}"
        # Look for method in imported modules
        imodule_names = tuple(_nms[1] for _nms in imported_modules) + tuple(
            _elm[0] for _elm in imported_elements
        )
        for module, (_, _, local_functions) in functions.items():
            if module in imodule_names:
                for function in local_functions:
                    if FunctionIsMethod(function) and (function["name"] == name):
                        return f"{module}.{function['defined_by']}.{name}"
        # Look for method among standard Python methods
        if name in BUILTIN_METHODS:
            return None
    else:
        print(
            f"{type(node).__name__}: "
            f"Unimplemented function type with attributes: "
            f"{ASTNodeToStr(node)}"
        )
        return None

    print(f"{name}: Unfound function called by {namespace}.{caller_name}")

    return None
