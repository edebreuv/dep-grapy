# Copyright CNRS/Inria/UCA
# Contributor(s): Eric Debreuve (since 2019)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import re as rgex
from typing import Iterable, Optional, Sequence, Tuple

import networkx as ntkx

from brick.app.defaults import (
    DEFAULT_SAVING_FORMAT,
    MAX_N_GRAPHS,
    MAX_N_NODES,
    MIN_N_NODES,
)
from brick.path import PathsAsRelative, path_t


GRAPH_NAMES = {"m": "module-graph", "c": "class-graph", "f": "function-graph"}
INTEGER_OPTIONS = ("max_n_graphs", "min_n_nodes", "max_n_nodes")
BOOLEAN_OPTIONS = ("sys_module", "sys_class")
GRAPH_LIST_OPTIONS = ("print", "show", "save")
GRAPH_FORMATS = {
    "GEXF": ("gexf", ntkx.readwrite.gexf.write_gexf),
    "GML": ("gml", ntkx.readwrite.gml.write_gml),
    "Graph6": ("g6", ntkx.readwrite.graph6.write_graph6),
    "GraphML": ("graphml", ntkx.readwrite.graphml.write_graphml),
    DEFAULT_SAVING_FORMAT: ("dot", ntkx.drawing.nx_agraph.write_dot),
    "Pajek": ("net", ntkx.readwrite.pajek.write_pajek),
    "Sparse6": ("g6", ntkx.readwrite.sparse6.write_sparse6),
}


def ProjectFolderAndFilesFromArguments(
    arguments: Sequence[str], /
) -> Tuple[Optional[path_t], Optional[Sequence[path_t]], Optional[str]]:
    """"""
    if arguments.__len__() < 2:
        return None, None, None

    project_folder = path_t(arguments[1]).resolve()
    if project_folder.is_dir():
        project_files = project_folder.rglob("*.py")
    elif project_folder.is_file():
        project_files = (project_folder,)
        project_folder = project_folder.parent
    else:
        return None, None, f"{project_folder}: Not a(n existing) folder or file"

    return project_folder, project_files, None


def OptionsFromArguments(
    arguments: Sequence[str], project_folder: path_t, /
) -> Tuple[
    Optional[Sequence[path_t]],
    Optional[Sequence[path_t]],
    bool,
    bool,
    int,
    int,
    int,
    Sequence[str],
    Sequence[str],
    Sequence[str],
    str,
    Sequence[str],
]:
    """"""
    include_list = []
    exclude_list = []
    include_sys_module = False
    include_sys_class = False
    max_n_graphs = MAX_N_GRAPHS
    min_n_nodes = MIN_N_NODES
    max_n_nodes = MAX_N_NODES
    printing_list = []
    showing_list = []
    saving_list = []
    saving_format = DEFAULT_SAVING_FORMAT
    issues = []

    for idx in range(2, arguments.__len__()):
        option = arguments[idx]
        if option.startswith("+"):
            include_list.append(option[1:])
        elif option.startswith("--"):
            name, value, issue = _NameAndValueOfInteger(option)
            if name is None:
                name, value, issue = _NameAndValueOfBoolean(option)
            if name is None:
                name, value, issue = _NameAndValueOfGraphList(option)
            if name is None:
                name, value, issue = _NameAndValueOfGraphFormat(option)

            if name is None:
                issues.append(f"{option}: Invalid option or option value")
            elif value is None:
                issues.append(issue)
            elif name == "sys_module":
                include_sys_module = value
            elif name == "sys_class":
                include_sys_class = value
            elif name == "max_n_graphs":
                max_n_graphs = value
            elif name == "min_n_nodes":
                min_n_nodes = value
            elif name == "max_n_nodes":
                max_n_nodes = value
            elif name == "print":
                printing_list = value
            elif name == "show":
                showing_list = value
            elif name == "save":
                saving_list = value
            elif name == "format":
                saving_format = value
            else:
                raise RuntimeError(
                    "This should not have happened; Please contact developer"
                )
        elif option.startswith("-"):
            exclude_list.append(option[1:])
    if max_n_graphs < 1:
        issues.append(f"{max_n_graphs}: Invalid maximum number of graphs; Must be >= 1")
    if min_n_nodes < 2:
        issues.append(f"{min_n_nodes}: Invalid minimum number of nodes; Must be >= 2")
    if max_n_nodes < min_n_nodes:
        issues.append(
            f"{max_n_nodes}: Invalid maximum number of nodes; Must be >= minimum number of nodes={min_n_nodes}"
        )

    if include_list is not None:
        if include_list.__len__() > 0:
            include_list = PathsAsRelative(include_list, project_folder)
        else:
            include_list = None
    if exclude_list is not None:
        if exclude_list.__len__() > 0:
            exclude_list = PathsAsRelative(exclude_list, project_folder)
        else:
            exclude_list = None

    return (
        include_list,
        exclude_list,
        include_sys_module,
        include_sys_class,
        max_n_graphs,
        min_n_nodes,
        max_n_nodes,
        printing_list,
        showing_list,
        saving_list,
        saving_format,
        issues,
    )


def _NameAndValueOfInteger(
    option: str, /
) -> Tuple[Optional[str], Optional[int], Optional[str]]:
    """"""
    return _NameAndValueOfOption(option, INTEGER_OPTIONS, "\d+", int, "integer")


def _NameAndValueOfBoolean(
    option: str, /
) -> Tuple[Optional[str], Optional[int], Optional[str]]:
    """"""
    BooleanFromStr = lambda _str: _str.startswith("y")
    return _NameAndValueOfOption(
        option, BOOLEAN_OPTIONS, "\w+", BooleanFromStr, '"y[es]/n[o]"'
    )


def _NameAndValueOfGraphList(
    option: str, /
) -> Tuple[Optional[str], Optional[int], Optional[str]]:
    """"""
    return _NameAndValueOfOption(
        option,
        GRAPH_LIST_OPTIONS,
        f"[{''.join(GRAPH_NAMES.keys())}]*",
        _GraphInitialsToGraphNames,
        "graph list",
    )


def _NameAndValueOfGraphFormat(
    option: str, /
) -> Tuple[Optional[str], Optional[int], Optional[str]]:
    """"""
    return _NameAndValueOfOption(
        option,
        ("format",),
        f"({'|'.join(GRAPH_FORMATS.keys())})",
        lambda _elm: _elm,
        "graph format",
    )


def _NameAndValueOfOption(
    option: str,
    valid_names: Iterable[str],
    value_pattern: str,
    InstanceFromStr: callable,
    type_as_str: str,
    /,
) -> Tuple[Optional[str], Optional[int], Optional[str]]:
    """"""
    name_pattern = "|".join(valid_names)
    match = rgex.search(
        f"^--(?P<name>{name_pattern})=(?P<value>{value_pattern})$", option
    )
    if match is None:
        name = value = issue = None
    else:
        name = match.group("name")
        value_as_str = match.group("value")
        try:
            value = InstanceFromStr(value_as_str)
            issue = None
        except:
            value = None
            issue = f'{value_as_str}: Invalid {type_as_str} value for "{name}"'

    return name, value, issue


def _GraphInitialsToGraphNames(initials: str, /) -> Sequence[str]:
    """"""
    output = [GRAPH_NAMES[_ltr] for _ltr in initials]

    return tuple(set(output))
