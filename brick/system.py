# Copyright CNRS/Inria/UCA
# Contributor(s): Eric Debreuve (since 2019)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import builtins as blts
import keyword as kywd
import site
import sys as syst

from brick.path import path_t


PY_VERSION = syst.version_info
SYS_PATH_PIECE = path_t("/lib") / f"python{PY_VERSION.major}.{PY_VERSION.minor}" / "?"
SYS_PATH_PIECE = str(SYS_PATH_PIECE)[:-1]

BUILTIN_TYPES = []
BUILTIN_FUNCTIONS = []
BUILTIN_METHODS = []
for builtin in dir(blts):
    if builtin in kywd.kwlist:
        continue

    try:
        instance = eval(builtin)
        # Test type first since types are also callable (used as constructors)
        if isinstance(instance, type):
            BUILTIN_TYPES.append(builtin)
            for attribute in dir(instance):
                if callable(getattr(instance, attribute)) and not attribute.startswith(
                    "__"
                ):
                    BUILTIN_METHODS.append(attribute)
        elif callable(instance) and not builtin.startswith("__"):
            BUILTIN_FUNCTIONS.append(builtin)
    except:
        pass
BUILTIN_METHODS = sorted(set(BUILTIN_METHODS))

INSTALLED_PACKAGE_FOLDER = tuple(
    path_t(_pth)
    for _pth in syst.path
    if _pth.startswith(syst.prefix) or _pth.startswith(syst.exec_prefix)
) + (site.getusersitepackages(),)
